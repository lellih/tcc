import React from 'react';
//import 'Menu.css';
//import { StyledMenu } from './Menu.styled.js';
//import { bool } from 'prop-types';

const Menu = () => {
  return (
    <ul className="menu" >
        <li>
            <a href="/register">
                
                Cadastro
            </a>
        </li>
        <li>
            <a href="/data-base">
                
                Banco de Dados
            </a>
        </li>
        <li>
            <a href="/logout">
                
                Sair
            </a>
        </li>
    </ul>
  )
}

//Menu.propTypes = {
//    open: bool.isRequired,
//}

export default Menu;