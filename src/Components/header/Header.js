import Nav from "./Nav";
//import ButtonMenu from "./ButtonMenu";
import Logo from "./Logo";
import User from "./User";
//import Burger from './Burger';
import React, { /*useState*/ } from 'react';
import Menu from './Menu';
import "./Header.css";

function Header(){

    //const [open, setOpen] = useState(false);

    return(
        <div className="Header">
            
            
            <Menu/>
            <User/>
            <Nav/>
            <Logo/>
        </div>
    );
}
export default Header;