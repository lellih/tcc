import React, {useContext} from 'react';
import Footer from './Footer.js';
import Header from './header/Header.js';
import WaitingList from './main/WaitingList/WaitingList.js';
import ToDoList from './main/ToDo/ToDoList';
import './Layout.css';
import actioncable from 'actioncable';
import { LayoutContext } from './Context.js';

const cableApp = actioncable.createConsumer('wss://db-svm.herokuapp.com/cable')

const Layout = (props) => {

    const {show} = useContext(LayoutContext)
    
    return(
        <React.Fragment>
            <Header/>
            
            <div className={show ? "main" : ""}>
                {show ? <WaitingList cable={cableApp}/> : null}
                    {props.children}
                {show ? <ToDoList cable={cableApp}/> : null}
                
            </div>
            <Footer/>
        </React.Fragment>
    );
};

export default Layout;