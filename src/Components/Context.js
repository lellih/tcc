import React, {createContext, useState} from 'react';

export const LayoutContext = createContext()
export const LayoutProvider = props => {
    const [show, setShow] = useState(true)
    
    return (
        <LayoutContext.Provider value={{show, setShow}}>{props.children}</LayoutContext.Provider>
    )
}