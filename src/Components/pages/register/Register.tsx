import React from 'react';
import Botao from './Botao';
import './register.css';
import {api} from "../../../Services/api.js";
import {Button} from 'react-bootstrap';
import { useState, useContext, useEffect } from "react";
import { useForm } from "react-hook-form";
import {LayoutContext} from '../../Context';

interface FormData {
  name: string;
  email: string;
  patent: string;
  password: string;
  password_confirmation: string;
  departamento: string;
  birthdate: string;
  phone: string;
  has_user: boolean;
}

export default function Cadastro() {

  const {setShow} = useContext(LayoutContext)
    useEffect(() => {
        setShow(true)
    }, [])
  
  const { register, handleSubmit, errors} = useForm<FormData>({
    defaultValues: {
      name:"",
      email:"",
      patent:"",
      password:"",
      password_confirmation:"",
      departamento:"",
      birthdate:"",
      phone:"",
      has_user: true,
    },
  });
  
  const [has_user, setHas_user] = useState<boolean>(true);
  const [submitting, setSubmitting] = useState<boolean>(false);

  return (
    <div>
      <section className = "formBack container">
        
        
        <h1>Cadastrar Tripulante</h1> 

        <div className="formBox">
          <div className="DivBotao">
            <label className="AreaTexto">*Usuário do sistema?</label>
            <Botao updateState={setHas_user}/>
          </div>
          <form
            className="formReg"
            onSubmit={handleSubmit(async (formData) => {
              console.log(has_user);
              setSubmitting(true);
              await api.post(
                "/auth/register",
                { 
                  "military": {
                    "name": formData.name,
                    "email": formData.email,
                    "patent": formData.patent,
                    "password": formData.password,
                    "password_confirmation": formData.password_confirmation,
                    "department": formData.departamento,
                    "birthdate": formData.birthdate,
                    "phone": formData.phone,
                    "has_user": !has_user
                  }
                },{
                  headers:{"Authorization": sessionStorage.getItem("token")}
                }
              ).then(() => alert("Cadastrado com sucesso!!"))
              .then(() => window.history.back())
              .catch(error => console.log(error));
              
                
              console.log(formData);
              setSubmitting(false);
            })}
          >
            
              <div>
                
                <div className="item">
                  <div className="formAtr">
                    <label htmlFor="name">Nome</label>
                    <input
                      type="text"
                      name="name"
                      placeholder="Nome de Guerra"
                      id="name"
                      ref={register({ required: "Necessário preencher" })}
                    />
                    {errors.name ? <div>{errors.name.message}</div> : null}
                  </div>

                  <div className="formAtr">
                    <label htmlFor="patent">Posto ou Graduação</label>
                    <select id="patent">
                      <option value=""></option>
                      <option value="CF">Capitão de Fragata</option>
                      <option value="CC">Capitão de Corveta</option>
                      <option value="CT">Capitão Tenente</option>
                      <option value="1T">Primeiro Tenente</option>
                      <option value="2T">Segundo Tenente</option>
                      <option value="SO">Suboficial</option>
                      <option value="1SG">Primeiro Sargento</option>
                      <option value="2SG">Segundo Sargento</option>
                      <option value="3SG">Terceiro Sargento</option>
                      <option value="CB">Cabo</option>
                      <option value="MN">Marinheiro</option>                      
                    </select>
                  </div>
                </div>
                
                <div className="item">
                  <div className="formAtr">
                    <label htmlFor="phone">Telefone</label>
                    <input
                      type="tel"
                      name="phone"
                      id="phone"
                      placeholder="xx xxxx-xxxx"
                      ref={register({
                        required: "Necessário preencher",
                        minLength: {
                          value: 11,
                          message: "Necessário no minimo 11 números",
                        },
                                    
                      })}
                    />
                    {errors.phone ? <div>{errors.phone.message}</div> : null}
                  </div>

                  <div className="formAtr">
                    <label htmlFor="email">*Email</label>
                    <input
                      type="email"
                      name="email"
                      id="email"
                      placeholder="Zimbra"
                      disabled= {has_user}
                      ref={register}                      
                    />
                    {errors.email ? <div>{errors.email.message}</div> : null}
                    
                  </div>
                </div>   

                <div className="item">       
                  <div className="formAtr">
                    <label htmlFor="password">*Senha</label>
                    <input
                      type="password"
                      name="password"
                      id="password"
                      placeholder="Mínimo de 8 caracteres"
                      disabled= {has_user}
                      ref={register}
                    />
                    {errors.password ? <div>{errors.password.message}</div> : null}
                  </div>

                  <div className="formAtr">
                    <label htmlFor="password_confirmation">*Confirmação de Senha</label>
                    <input
                      type="password"
                      name="password_confirmation"
                      id="password_confirmation"
                      placeholder="Repita a senha"
                      disabled= {has_user}
                      ref={register}
                    />
                    {errors.password_confirmation ? <div>{errors.password_confirmation.message}</div> : null}
                  </div>
                </div>
                <div className="item">       
                  <div className="formAtr">
                    <label htmlFor="birthdate">Data de Nascimento</label>
                    <input
                      type="text"
                      name="birthdate"
                      id="birthdate"
                      placeholder="DD/MM/AAAA"
                      ref={register({
                        required: "Necessário preencher",
                        minLength: {
                          value: 10,
                          message: "Preencha no formato DD/MM/AAAA ",
                        },
                      })}
                    />
                    {errors.birthdate ? <div>{errors.birthdate.message}</div> : null}
                  </div>

                  <div className="formAtr">
                    <label htmlFor="departamento">Seção/Departamento</label>
                    <input
                      type="text"
                      name="departamento"
                      id="departamento"
                      placeholder="Nome do setor"
                      ref={register}
                      
                    />
                    
                  </div>
                </div>       
            </div>   

            <div className="formBtn">
              <Button className="modalBtn" data-toggle="button" type="submit" disabled={submitting}>
                Salvar
              </Button>

            </div>  

          </form>

        </div>
      </section>
    </div>
  );
}
