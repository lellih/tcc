import React from 'react';

export default class Usuario extends React.Component{

    render(){
        return(
            <div className={"Usuário do sistema? " + this.props.classeSecundaria}>{this.props.texto}</div>
        );
    }
}