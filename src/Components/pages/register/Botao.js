import React from 'react';

class Botao extends React.Component {
    constructor(props) {
      super(props);
      this.state = { isToggleOn: true };
  
      // Aqui utilizamos o `bind` para que o `this` funcione dentro da nossa callback
      this.handleClick = this.handleClick.bind(this);
    }
  
    handleClick() {
      this.setState(state => ({
        isToggleOn: !state.isToggleOn
      }));
      this.props.updateState(!this.state.isToggleOn)
      console.log(this.state);
    }

  
    render() {
      return (
        <button onClick={this.handleClick}>
          {this.state.isToggleOn ? 'NÃO' : 'SIM'}
        </button>
      );
    }
  }
  
  export default Botao;