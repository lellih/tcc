import React, { useContext, useEffect, useState } from 'react';
import '../main/Calendar/Calendar.css';
import { LayoutContext } from '../Context';
import CalendarWindow from '../main/Calendar/CalendarWindow';
import AgendaModal from '../main/Calendar/AgendaModal';

export default function Agenda(props){

    const [dropdown, setDropdown] = useState("");

    const showDropdown = () => {
        console.log("show");
        setDropdown("show");
    }

    const closeDropdown = event => {
        console.log("hidden");
        setDropdown("");
        
    };

    const {setShow} = useContext(LayoutContext)
    useEffect(() => {
        setShow(true)
    }, [])

    return(
        <div className='Agenda'>
            <div className="add-agenda">
                <h2>Agenda</h2>
                <button onClick={showDropdown}>+</button>
            </div>
            <AgendaModal state={dropdown} close={closeDropdown}/>
            <CalendarWindow commitments={props.commitments}/>
            
                    
                
            
        </div>
    );
    
}