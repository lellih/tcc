import React, { useContext, useEffect } from 'react';
import { LayoutContext } from '../Context.js';
import ToDoBox from '../main/ToDo/ToDoBox.js';
import WorkSpace from '../main/WorkSpace';
import '../styles/styles.css';
import ToDoArray from '../main/ToDo/ToDoArray';
import WebSocketComponent from '../../Services/WebSocketComponent';
import actioncable from 'actioncable';

const cable = actioncable.createConsumer("wss//db-svm.herokuapp.com/cable/")
    

export default function Tarefas(props){

    const {setShow} = useContext(LayoutContext)
    useEffect(() => {
        setShow(false)
    }, [])

    
    
    return(
        <div className='Tarefas'>
            <h1>Quadro de Tarefas</h1>
            <WorkSpace>
                <WebSocketComponent 
                    cable={cable}
                    updateApp={props.updateAppState}
                    room="tasks"
                />
                <div className="colunas">
                    <div className="box-title">
                        <h3>Fazer</h3>
                        <div className="fazer">
                            
                            <ToDoArray list={props.aFazer}/>
                        </div>
                    </div>
                    <div className="box-title">
                        <h3>Em Andamento</h3>
                        <div className="andamento">
                            
                            <ToDoArray list={props.andamento}/>
                        </div>
                    </div>
                    <div className="box-title">
                        <h3>Aguardando Aprovação</h3>
                        <div className="aprovar">
                            
                            <ToDoArray list={props.validacao}/>
                        </div>
                    </div>
                    <div className="box-title">
                        <h3>Pronto</h3>
                        <div className="pronto">
                            
                            <ToDoArray list={props.feito}/>
                        </div>
                    </div>
                </div>
                <ToDoBox/>
            </WorkSpace>
        </div>
    );
    
}
