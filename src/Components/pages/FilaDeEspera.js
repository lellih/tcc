import React, { useContext, useEffect } from 'react';
import WorkSpace from '../main/WorkSpace';
import WaitingForm from '../main/WaitingList/WaitingForm';
import { LayoutContext } from '../Context';

export default function FilaDeEspera (){

    const {setShow} = useContext(LayoutContext)
    useEffect(() => {
        setShow(true)
    }, [])

    return(
        <div className='Fila-de-espera'>
            <h1>Fila de Espera</h1>
            <WorkSpace>
                <WaitingForm/>
            </WorkSpace>
        </div>
    );
    
}
