import React from 'react';
import WorkSpace from '../main/WorkSpace';

const CommitmentView = props => {
  let dia = window.location.href.toString().split("/")[4]
  let mes = window.location.href.toString().split("/")[5]
  let ano = window.location.href.toString().split("/")[6]
  
  let this_name = dia + "/" + mes + "/" + ano 
  return (

    <div className='Compromissos'>
        <h1>Compromissos</h1>
        <WorkSpace>
        <h2>{this_name}</h2>
        {props.commitments.map(element => {
            if(element.day_of === this_name){
            return (
              <div className="Evento">
                <h3 key={element.id}>{element.description}</h3>
                <p key={element.id}>{element.note}</p>
                <p key={element.id}>{element.time_init}</p>
              </div>)
            }
        })}
        </WorkSpace>
    </div>
  )

}

export default CommitmentView