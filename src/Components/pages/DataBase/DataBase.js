import React, {useContext, useEffect, useState} from 'react';
import {api} from '../../../Services/api';
import { LayoutContext } from '../../Context';
import WorkSpace from '../../main/WorkSpace';
import './DataBase.css';
import DBModal from './DBModal';


const DataBase = () => {
    
    const [militaries, setMilitaries] = useState([]);
    //const [militarEdit, setMilitarEdit] = useState({});
    const [dropdown, setDropdown] = useState("");

    const showDropdown = () => {
        console.log("show");
        setDropdown("show");
    }

    const closeDropdown = event => {
        console.log("hidden");
        setDropdown("");
        
    };

    const {setShow} = useContext(LayoutContext)
    useEffect(() => {
        setShow(true)
    }, [])

    useEffect(() => {
        api.get('/militaries', {
            headers: {
                "Authorization": sessionStorage.getItem("token")
            }
        }).then(data => setMilitaries(data.data))
    }, [])

    return(
        <div className='DataBase'>
            <h1>Banco de Dados</h1>
            <WorkSpace>
                <DBModal state={dropdown} close={closeDropdown}/>
                {militaries.map((militar) => (
                    <div className="edit-db">
                        
                        <button onClick={showDropdown} user={militar}>Editar</button>
                        <p>{militar.name}</p>
                    </div>
                    
                ))}
            </WorkSpace>
        </div>
    );
    
}

export default DataBase;