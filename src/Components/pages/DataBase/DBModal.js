import React, { useState } from 'react';
import "./DataBase.css";
import {api} from '../../../Services/api';
import '../../main/WaitingList/WaitingForm.css';
import Botao from  '../register/Botao';

const DBModal = props => {
    
    const [name, setName] = useState(props.name)
    const [email, setEmail] = useState(props.email)
    const [patent, setPatent] = useState(props.patent)
    const [password, setPassword] = useState(props.password)
    const [password_confirmation, setPassword_confirmation] = useState(props.password_confirmation)
    const [department, setDepartment] = useState(props.department)
    const [birthdate, setBirthdate] = useState(props.birthdate)
    const [phone, setPhone] = useState(props.phone)
    const [has_user, setHas_user] = useState(props.has_user)

    const handlerSubmit = e => {
        e.preventDefault()
        api.post("/auth/register", {
        "user":{
            "name":name,
            "patent":patent,
            "password": password,
            "email": email,
            "password_confirmation": password_confirmation,
            "department": department,
            "birthdate": birthdate,
            "phone": phone,
            "has_user": has_user
        }
        },{
            headers: {
                "Authorization": sessionStorage.getItem('token')
            }
        }).then(() => {
            alert("Dados Salvos!");
            setName("");
            setPatent("");
            setPassword("");
            setEmail("");
            setPassword_confirmation("");
            setDepartment("");
            setBirthdate("");
            setPhone("");
            setHas_user("");
        })

    
    }

    return(
        <div className={props.state+' DB-modal'}>
            <button className="close-button" onClick={props.close}>X</button>
            <div className= 'formView'>
                
                <div className="DivBotao">
                    <label className="AreaTexto">*Usuário do sistema?</label>
                    <Botao updateState={setHas_user}/>
                </div>
                <br/><br/>
                <form className="formClass" onSubmit={handlerSubmit}>
                    <div>
                        <label>Nome</label>
                        <input className="input-modal" value = {name} onChange={e=>setName(e.target.value)}/>
                    </div>
                    <div>
                        <label>E-mail</label>
                        <input type="email" disabled= {has_user} className="input-modal" value = {email} onChange={e=>setEmail(e.target.value)}/>
                    </div>
                    <div>
                        <label>Posto/Graduação</label>
                        <select className="input-modal"  value = {patent} onChange={e=>setPatent(e.target.value)}>
                            <option value="CF">CF</option>
                            <option value="CC">CC</option>
                            <option value="CT">CT</option>
                            <option value="1T">1T</option>
                            <option value="2T">2T</option>
                            <option value="SO">SO</option>
                            <option value="1SG">1SG</option>
                            <option value="2SG">2SG</option>
                            <option value="3SG">3SG</option>
                            <option value="CB">CB</option>
                            <option value="MN">MN</option>
                        </select>
                    </div>
                    <div>
                        <label>Aniversário</label>
                        <input className="input-modal" placeholder="dd/mm/aaaa" value = {birthdate} onChange={e=>setBirthdate(e.target.value)}/>
                    </div>
                    <div>
                        <label>Senha</label>
                        <input className="input-modal" type="password" disabled= {has_user} value = {password} onChange={e=>setPassword(e.target.value)}/>
                    </div>
                    <div>
                        <label>Confirmação de senha</label>
                        <input className="input-modal" type="password" disabled= {has_user} value = {password_confirmation} onChange={e=>setPassword_confirmation(e.target.value)}/>
                    </div>
                    <div>
                        <label>Seção</label>
                        <input className="input-modal" placeholder="ex.: 011" value = {department} onChange={e=>setDepartment(e.target.value)}/>
                    </div>
                    <div>
                        <label>Contato</label>
                        <input className="input-modal" placeholder="ddd+numero" value = {department} onChange={e=>setDepartment(e.target.value)}/>
                    </div>
                    <button className="DB-button" onClick={handlerSubmit}>Salvar</button>
                </form>            
            </div>
        </div>
    )
}

export default DBModal;