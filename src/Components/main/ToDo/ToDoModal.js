import React, { useState } from 'react';
import "./ToDoModal.css";
import {api} from '../../../Services/api';
import '../WaitingList/WaitingForm.css';

const ToDoModal = props => {
    
    const [description, setDescription] = useState("")
    const [date_limit, setDate_limit] = useState("")
    const [priority, setPriority] = useState("")

    const handlerSubmit = e => {
        e.preventDefault()
        api.post("/tasks", {
        "task":{
            "description":description,
            "limit":date_limit,
            "priority": priority
        }
        },{
            headers: {
                "Authorization": sessionStorage.getItem('token')
            }
        }).then(() => {
            alert("Criada");
            setDescription("");
            setDate_limit("");
            setPriority("");
            
        })

    
    }

    return(
        <div className={props.state+' tarefa-modal'}>
            <button className="close-button" onClick={props.close}>X</button>
            <div className= 'formView'>
            <br/><br/>
            <form className="ToDo-formClass" onSubmit={handlerSubmit}>
                <div>
                    <label>Descrição</label>
                    <input className="input-modal" value = {description} onChange={e=>setDescription(e.target.value)}/>
                </div>
                <div>
                    <label>Prazo</label>
                    <input className="input-modal" placeholder="dd/mm/aaaa" value = {date_limit} onChange={e=>setDate_limit(e.target.value)}/>
                </div>
                <div>
                    <label>Nivel de prioridade</label>
                    <input className="input-modal" placeholder="0:Menos importante 1:Mediano 2:Muito importante" value = {priority} onChange={e=>setPriority(e.target.value)}/>
                </div>
                <button className="input-button" onClick={handlerSubmit}>Criar Tarefa</button>
            </form>            
            </div>
        </div>
    )
}

export default ToDoModal;