import './ToDoList.css';
import React, { useState, useEffect } from 'react'
import {api} from '../../../Services/api';
import WebSocketComponent from '../../../Services/WebSocketComponent'

export default function ToDoList(props){

    const [tasks, setTasks] = useState([])
    const updateAppState = (meets) => {
        console.log(meets);
        setTasks(meets)
    }

    useEffect(() => {
        api.get("/tasks", {
            headers: {
                "Authorization": sessionStorage.getItem('token')
            }
        })
        .then(resp => {
            setTasks(resp.data)
        })
    }, [])
    
    return(
        
        <div>
            <h2>Tarefas</h2>
            <div className="ToDoList">
                
                <WebSocketComponent 
                cable={props.cable}
                updateApp={updateAppState}
                room="tasks"
                />

                {tasks.map(task => (
                    <div key = {task.id} id={task.id} className={task.state + " listItem"}>
                        <h3>{task.description}</h3>
                        <p>{task.limit}</p>
                        <p>prioridade = {task.priority}</p>
                    </div>
                ))}
            </div>
        </div>

    );
    
}

