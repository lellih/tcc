import React from 'react';
import './ToDoBox.css';
import {api} from '../../../Services/api';

export default function ToDoArray(props){

    var possiblePhases = ["Fazer", "Andamento", "Validação", "Feito"]

    
    const handleSubmit = event => {
        event.preventDefault();
        
        api.put('/tasks/'+event.target.id.split(" ")[0], {
            "task":{
                "phase": parseInt(event.target.id.split(" ")[1])+1,
            }
        },{
            headers: {
                "Authorization": sessionStorage.getItem('token')
            }
        })
        
    }

    return(
        <div className="tasks-array">
            
            {props.list.map(element => {
                let indexPhase = possiblePhases.indexOf(element.phase) 
                
                return (
                    
                    <div className="elementPhase1">
                        <h3> {element.description}</h3>
                        <p> {element.limit}</p>
                        <p> prioridade: {element.priority} </p>
                        <button id={element.id + " " + indexPhase} onClick={handleSubmit}>Avançar</button>
                    </div>
                )
                
            })}
        </div>
    )
}