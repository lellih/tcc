import React, { useState } from 'react';
import ToDoModal from './ToDoModal';
import "./ToDoBox.css";

const ToDoBox = (props) => {
    const [dropdown, setDropdown] = useState("");

    const showDropdown = () => {
        console.log("show");
        setDropdown("show");
    }

    const closeDropdown = event => {
        console.log("hidden");
        setDropdown("");
        
    };
    
    return (
        <div className="ToDo">
            <button onClick={showDropdown}>Criar Tarefa</button>
            <ToDoModal state={dropdown} close={closeDropdown}/>
        </div>
    )
}

export default ToDoBox