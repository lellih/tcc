import React, { useState } from 'react';
import {api} from '../../../Services/api';
import './WaitingForm.css';

const WaitingForm = (props) => {
  const [name, setname] = useState("")
  const [previsted_time, setprevisted_time] = useState("")
  const [subject, setsubject] = useState("")
  const [priority, setpriority] = useState("")
  const [companions, setcompanions] = useState("")

  const handlerSubmit = e => {
    e.preventDefault()
    api.post("/meetings", {
      "meeting":{
        "name":name,
        "subject":subject,
        "previsted_time":previsted_time,
        "priority": priority,
        "companions":companions
      }
    },{
        headers: {
            "Authorization": sessionStorage.getItem('token')
        }
    }).then(() => {
      
      alert("Criado");
      setname("");
      setprevisted_time("");
      setsubject("");
      setpriority("");
      setcompanions("");
    })
  }

  const deleteAll = () => {
    api.delete("meetings/all", {
        headers: {
            "Authorization": sessionStorage.getItem('token')
        }
    })
    
  }

  return(
    <div className= 'formView'>
      <br/><br/>
      <form className="formClass" onSubmit={handlerSubmit}>
        <div>
          <label>Nome</label>
          <input className="input-form" placeholder="Posto/Graduação/Nome de guerra" value = {name} onChange={e=>setname(e.target.value)}/>
        </div>
        <div>
          <label>Tempo Previsto</label>
          <input className="input-form" placeholder="Em minutos" value = {previsted_time} onChange={e=>setprevisted_time(e.target.value)}/>
        </div>
        <div>
          <label>Assunto</label>
          <input className="input-form" placeholder="Pauta da reunião" value = {subject} onChange={e=>setsubject(e.target.value)}/>
        </div>
        <div>
          <label>Nivel de prioridade</label>
          <input className="input-form" placeholder="2:Urgente - 1:Hoje - 0:Pode esperar mais" value = {priority} onChange={e=>setpriority(e.target.value)}/>
        </div>
        <div>
          <label>Número de pessoas</label>
          <input className="input-form" placeholder="0 se não estiver acompanhado" value = {companions} onChange={e=>setcompanions(e.target.value)}/>
        </div>
        <input className="button-form" type="submit" value="Enviar nova reunião"/>
      </form>
      <button className="button-form" onClick={deleteAll}>Deletar todas as reuniões</button>
    </div>
  )
}

export default WaitingForm