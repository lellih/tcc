import './WaitingList.css';
import React, { useState, useEffect } from 'react'
import {api} from '../../../Services/api';
import WebSocketComponent from '../../../Services/WebSocketComponent'
import ButtonsView from './ButtonsView';

export default function WaitingList(props){

    const [meetings, setmeetings] = useState([])
    const updateAppState = (meets) => {

        setmeetings(meets)
    }

    useEffect(() => {
        api.get("/meetings", {
            headers: {
                "Authorization": sessionStorage.getItem('token')
            }
        })
        .then(resp => {
            setmeetings(resp.data)
        })
    }, [])
    
    return(
        
        <div>
            <h2>Fila de Espera</h2>
            <div className="WaitingList">
                
                <WebSocketComponent 
                cable={props.cable}
                updateApp={updateAppState}
                room="meetings"
                />

                {meetings.map(meet => (
                    <div key = {meet.id} id={meet.id} className={meet.state + " waiting-listItem"}>
                        <div className="waiting-info">
                            <h3>{meet.name}</h3>
                            <p>{meet.subject}</p>
                            <p>prioridade = {meet.priority}</p>
                        </div>
                        <ButtonsView state = {meet.state}/>
                    </div>
                ))}
            </div>
        </div>

    );
    
}

