import React from 'react';
import {api} from '../../../Services/api';
import './WaitingList.css';


const ButtonsView = (props) => {
  const iniciateMeeting = e => {
    api.put('meetings/'+e.target.parentNode.id, {
      meeting:{
        "state":"Andamento"
      }
    },{
        headers: {
            "Authorization": sessionStorage.getItem('token')
        }
    })
  }
  
  const closeMeeting = e => {
    api.put('meetings/'+e.target.parentNode.id, {
      meeting:{
        "state":"Concluida"
      }
    },{
        headers: {
            "Authorization": sessionStorage.getItem('token')
        }
    })
  }

  const reopenMeeting = e => {
    api.put('meetings/'+e.target.parentNode.id, {
      meeting:{
        "state":"Aguardando"
      }
    },{
        headers: {
            "Authorization": sessionStorage.getItem('token')
        }
    })
  }

  const cancelMeeting = e => {
    api.put ('meetings/'+e.target.parentNode.parentNode.id, {
      meeting:{
        "state":"Cancelada"
      }
    },{
        headers: {
            "Authorization": sessionStorage.getItem('token')
        }
    })
  }
  const letItIn = e => {
    api.put ('meetings/'+e.target.parentNode.parentNode.id, {
      meeting:{
        "state":"Entrar"
      }
    },{
        headers: {
            "Authorization": sessionStorage.getItem('token')
        }
    })
  }

  switch (props.state) {
    case "Entrar":
      return (
        <button onClick ={iniciateMeeting}>Iniciar</button>
      );
    case "Andamento":
      return (
        <button onClick ={closeMeeting}>Concluir reunião</button>
      );
    case "Cancelada":
      return(
        <button onClick ={reopenMeeting}>Voltar para a fila</button>
      )
    case "Concluida":
      return null
    default:
      return (
        <div className="btn-entrar-cancel">
          <button onClick={cancelMeeting} className="Cancel">Cancelar</button>
          <button onClick={letItIn} className="Come-In">Entrar</button>
        </div>  
      )
  }
}

export default ButtonsView