import React, { useState } from 'react';
import "./Calendar.css";
import {api} from '../../../Services/api';
import '../WaitingList/WaitingForm.css';

const AgendaModal = props => {
    
    const [description, setDescription] = useState("")
    const [note, setNote] = useState("")
    const [date_init, setDate_init] = useState("")
    const [date_end, setDate_end] = useState("")
    const [time_init, setTime_init] = useState("")
    const [time_end, setTime_end] = useState("")
    const [repeater, setRepeater] = useState("never")

    const handlerSubmit = e => {
        e.preventDefault()
        api.post("/commitments", {
        "commitment":{
            "description":description,
            "date_init":date_init,
            "date_end": date_end,
            "note": note,
            "time_init": time_init,
            "time_end": time_end,
            "repeater": repeater
        }
        },{
            headers: {
                "Authorization": sessionStorage.getItem('token')
            }
        }).then(() => {
            alert("Agendado");
            setDescription("");
            setDate_init("");
            setDate_end("");
            setNote("");
            setTime_init("");
            setTime_end("");
            setRepeater("");
        })

    
    }

    return(
        <div className={props.state+' agenda-modal'}>
            <button className="close-button" onClick={props.close}>X</button>
            <div className= 'formView'>
            <br/><br/>
            <form className="formClass" onSubmit={handlerSubmit}>
                <div>
                    <label>Descrição</label>
                    <input className="input-modal" value = {description} onChange={e=>setDescription(e.target.value)}/>
                </div>
                <div>
                    <label>Observações</label>
                    <input type="textarea" className="input-modal" value = {note} onChange={e=>setNote(e.target.value)}/>
                </div>
                <div>
                    <label>Data</label>
                    <input className="input-modal" placeholder="dd/mm/aaaa" value = {date_init} onChange={e=>setDate_init(e.target.value)}/>
                </div>
                <div>
                    <label>Repetir</label>
                    <select className="input-modal" value = {repeater} onChange={e=>setRepeater(e.target.value)}>
                        <option value="never">Nunca</option>
                        <option value="daily">Diariamente</option>
                        <option value="weekly">Semanalmente</option>
                        <option value="monthly">Mensalmente</option>
                        <option value="annualy">Anualmente</option>
                    </select>
                </div>
                <div>
                    <label>*Lembrar até</label>
                    <input className="input-modal" placeholder="dd/mm/aaaa (caso for repetir)" value = {date_end} onChange={e=>setDate_end(e.target.value)}/>
                </div>
                <div>
                    <label>Hora de início</label>
                    <input className="input-modal" placeholder="hh:mm" value = {time_init} onChange={e=>setTime_init(e.target.value)}/>
                </div>
                <div>
                    <label>Hora de término</label>
                    <input className="input-modal" placeholder="hh:mm" value = {time_end} onChange={e=>setTime_end(e.target.value)}/>
                </div>
                <button className="agenda-button" onClick={handlerSubmit}>Agendar Compromisso</button>
            </form>            
            </div>
        </div>
    )
}

export default AgendaModal;