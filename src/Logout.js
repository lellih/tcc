import React from 'react';
import './Logout.css';
import WorkSpace from './Components/main/WorkSpace';
//import Login from './Login';

export default function Logout(){

    const handleClick = () => {
        sessionStorage.removeItem('token')
        window.location.reload()
        alert("Você foi deslogado")
    }

    return(
        <div className='logout'>
            <h1>Sair</h1>
            <WorkSpace>
                <div className='logout-box'>
                    <h3>Deseja SAIR da sessão?</h3>
                    <button onClick={handleClick}>SIM</button>
                </div>
            </WorkSpace>
        </div>
    );
    
}