import React, {useEffect, useState} from 'react'; 
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'; 
import Login from '../Login';
import Logout from '../Logout';
import Forgotten from '../Forgotten'; 
import Register from '../Components/pages/register/Register.tsx';
import Agenda from '../Components/pages/Agenda';
import Compromissos from '../Components/pages/Compromissos';
import DataBase from '../Components/pages/DataBase/DataBase';
import FilaDeEspera from '../Components/pages/FilaDeEspera';
import Tarefas from '../Components/pages/Tarefas';
import Layout from '../Components/Layout'; //componente usado para fixar o que deve ser sempre exibido da tela e só mudar o miolo desejado
import {LayoutProvider} from '../Components/Context'; //usado para omitir partes do layout em páginas específicas
import {api} from './api'; //usado para requisições

//componente que associa e redireciona os demais componentes para cada URL
const Routes = () => {

    //state a ser enviado para a página da agenda
    const [commitments, setCommitments] = useState([])
    useEffect (() => {
        api.get("/commitments")
        .then(resp => {
        setCommitments(resp.data)      
        })
    }, [])

    //estados usados para renderização no quadro de tarefas
    const [aFazer, setAFazer] = useState([])
    const [andamento, setAndamento] = useState([])
    const [validacao, setValidacao] = useState([])
    const [feito, setFeito] = useState([])
    
    
    const updateAppState = (meets) => {
        console.log(meets);
        handleLists(meets)
    }
    
    //requisição a ser enviada como estados na página de tarefas
    useEffect(() => {
        api.get('/tasks', {headers:{"Authorization":sessionStorage.getItem('token')}})
        .then(resp => handleLists(resp.data))}, []
    )

    const handleLists = arrayTasks => {
        
        arrayTasks.forEach(element => {
            console.log(element);
            if (element.phase === "Fazer"){
                let newList  = aFazer;
                newList.push(element);
                setAFazer(newList);
            }else if (element.phase === "Andamento"){
                let newList  = andamento;
                newList.push(element);
                setAndamento(newList);
            }else if (element.phase === "Validação"){
                let newList  = validacao;
                newList.push(element);
                setValidacao(newList);
            }else if (element.phase === "Feito"){
                let newList  = feito;
                newList.push(element);
                setFeito(newList);
            }
        }); 
    }
    

    return(
        //só entra no switch se estiver logado
        sessionStorage.getItem("token") ? (
            <BrowserRouter>
                <LayoutProvider>
                    
                    <Layout>
                        
                        <Switch>
                            <Route exact path="/login">
                                {sessionStorage.getItem("token") ? <Redirect to="/" /> : <Redirect to="/login" />}
                                
                            </Route>
                            <Route path="/register">
                                {sessionStorage.getItem("token") ? <Redirect to="/register" /> : <Redirect to="/login" />}
                                <Register />
                            </Route>
                            <Route path="/data-base">
                                {sessionStorage.getItem("token") ? <Redirect to="/data-base" /> : <Redirect to="/login" />}
                                <DataBase />
                            </Route>
                            <Route exact path="/">
                                {sessionStorage.getItem("token") ? <Redirect to="/" /> : <Redirect to="/login" />}
                                <Agenda commitments={commitments}/>
                            </Route>
                            <Route path="/compromissos">
                                {sessionStorage.getItem("token") ? <Redirect to="/compromissos" /> : <Redirect to="/login" />}
                                <Compromissos commitments={commitments}/>
                            </Route>
                            <Route path="/fila-de-espera">
                                {sessionStorage.getItem("token") ? <Redirect to="/fila-de-espera" /> : <Redirect to="/login" />}
                                <FilaDeEspera />
                            </Route>
                            <Route path="/tarefas">
                                {sessionStorage.getItem("token") ? <Redirect to="/tarefas" /> : <Redirect to="/login" />}
                                <Tarefas updateAppState={updateAppState} aFazer={aFazer} andamento={andamento} validacao={validacao} feito={feito}/>
                            </Route>
                            <Route path="/logout">
                                <Logout />
                            </Route>
                        </Switch>
                    </Layout>
                </LayoutProvider>
            </BrowserRouter>
        ) : (
            <BrowserRouter>
                <Redirect to="/login"/>
                <Route exact path="/login">
                    <Login />
                </Route>
                <Route path="/forgotten">
                    <Forgotten />
                </Route>
            </BrowserRouter>
        )
    )
}
export default Routes;