import { getToken } from "./auth";
import axios from "axios";

const api = axios.create({
  baseURL: "https://db-svm.herokuapp.com"
});

api.interceptors.request.use(async config => {
  const token = getToken();
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

async function forgot(formState) {
  await api.post("/login", formState)
    .then( response => {
      //localStorage.setItem('user', JSON.stringify(response.data))
      //check_admin()
    }).catch ( error => {
      window.alert("Não foi possível fazer o login. Verifique suas informações e tente novamente")
    })
}

export { 
  //check_admin,
  api,
  forgot
}