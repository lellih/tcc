import React, { useEffect } from 'react'

const WebSocketComponent = (props) => {
  useEffect(() => {
    props.cable.subscriptions.create({
      channel: "IssuesChannel",
      room: props.room
    },
    {
      received: (meeting) =>{
        console.log(meeting)
        props.updateApp(meeting)
      }
    }
    )
    
  }, [])

  return (
    <div></div>
  )
}
export default WebSocketComponent