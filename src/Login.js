import React, {} from 'react';
import './Login.css';
import logo from './Components/header/brasao-sem-fundo.png';
import {api} from './Services/api';
import { Redirect, Link } from 'react-router-dom';


const validateForm = (errors) => {
  let valid = true;
  Object.values(errors).forEach(
    (val) => val.length > 0 && (valid = false)
  );
  return valid;
}

class Login extends React.Component { 

  state = {
    email:"",
    senha:"",
    isChecked: false,
    redirect:false,
    errors: {
      email:"",
      senha:"",
    }
  }

  
  
  handleChange = (event) => {
    event.preventDefault();
    const { name, value } = event.target;
    let errors = this.state.errors;

    switch (name) {
      case 'email': 
        errors.email = value.length < 11 ? 'O Email é inválido!' : '';
        break;
      
      case 'senha': 
        errors.senha = value.length < 8 ? 'A senha deve ter no mínimo 8 caracteres' : '';
        break;

      default:
        break;
    }
    this.setState({errors, [name]: value});
  }

  
  onChangeCheckbox = event => this.setState({isChecked: event.target.checked})

  render () {
    const { senha, email, isChecked, errors} = this.state
    
      return (
        <div>
          <section id="loginPage" className="container">
            <form className="login-form" onSubmit ={this.handleSubmit} noValidate>
              <img src={logo} alt="logo" /> 
              <h1>Seja bem vindo!</h1>
              <h2>Faça login para continuar.</h2>
              <div className="loginBg">
                <div>
                  <div className="loginAtr">
                    
                    <input 
                    onChange={this.handleChange} 
                    name="email" 
                    placeholder="E-mail"
                    type="email"
                    value={email} 
                    noValidate
                    required/>
                      {errors.email.length > 0 && 
                    <span className='error'>{errors.email}</span>}
                  </div>

                  <div className="loginAtr">
                    <input 
                    name="senha" 
                    onChange={this.handleChange} 
                    placeholder="Senha" 
                    type="password" 
                    value={senha}
                    //noValidate
                    required />
                    {errors.senha.length > 0 && 
                    <span className='error'>{errors.senha}</span>}
                  </div>

                  <div className="checkbox">
                    <form Check 
                    type="checkbox" 
                    label="Lembrar de mim" 
                    name="lsRememberMe"
                    checked={isChecked} 
                    onChange={this.onChangeCheckbox}
                    />
                  </div>
                </div> 
              </div>
              <div className="btn">
                <div>
                  <button className="btn-preset" type='submit'>Login</button>
                </div>
                <Link to="/forgotten">Esqueceu sua senha? Clique aqui</Link>
              </div>                        
            </form>
          </section>
        </div>
      )
      
  }
  
  handleSubmit = event => {
    const {senha, email, isChecked} = this.state
    
    event.preventDefault()

    if(validateForm(this.state.errors)) {
      
           
      api.post("/auth/login", {"user": {
        "email":email,
        "password" :senha
        }})
        .then(function(data) {
          sessionStorage.setItem('token', data.data.token);
          sessionStorage.setItem('kind', data.data.user.kind);
          var {user} = data.data;
          localStorage.setItem('user', user);
          

          if (isChecked && email !== "") {
            localStorage.setItem('token', data.token)
          };
        })   
        .then(() => window.location.href = "/")
        .catch (error => <Redirect to="/forgotten"/>)
    
    }
    else{
        console.error('formulário invalido')
    }        
  
  }
}

export default Login;