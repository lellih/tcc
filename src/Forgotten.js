import React from 'react';
import * as api from './Services/api.js';
import {Link } from "react-router-dom";
import logo from './Components/header/brasao-sem-fundo.png';

const validateForm = (errors) => {
    let valid = true;
    Object.values(errors).forEach(
      (val) => val.length > 0 && (valid = false)
    );
    return valid;
  }


class Forgotten extends React.Component { 

    state = {
        email:"",
        redirect:false,
        errors: {
            email:"",
        }
    }
    
    handleChange = (event) => {
        event.preventDefault();
        const { name, value } = event.target;
        let errors = this.state.errors;
    
        switch (name) {
          case 'email': 
            errors.email = 
              value.length < 11
                ? 'O e-mail é inválido!'
                : '';
            break;
         
          default:
            break;
        }
        this.setState({errors, [name]: value});
    }

    render () {
        const {email, errors} = this.state
       
            return (
                <React.Fragment>
                    <section id="forgotPage" className="container">
                        <form className="forgotBox" onSubmit ={this.handleSubmit} noValidate>
                            <img src={logo} alt="logo" /> 
                            <p>Por favor, insira seu e-mail. Você receberá um link para criar uma nova senha por ele.</p>
                            <div className="forgotAtr">
                                <label htmlFor="cpf">Seu e-mail</label>
                                <input 
                                onChange={this.handleChange}
                                name="email" 
                                placeholder="Seu e-mail"
                                type="email"
                                value={email} 
                                noValidate
                                required/>
                                {errors.email.length > 0 && 
                                <span className='error'>{errors.email}</span>}        

                            </div>
                            <div className="forgotButtons">
                                <button variant="primary" className="forgotBtn" type='submit'>Recuperar Senha</button>
                                <Link to="/login">Voltar à página de Login</Link>
                            </div>                        
                        </form>
                     </section>
            </React.Fragment>
            )
        
    }
    

    handleSubmit = event => {
        const {email} = this.state
        
        event.preventDefault()
        
        if(validateForm(this.state.errors)) {
            api.forgot('password/forgot',{
                "email":email
                })

            .then(resp => {
                alert("Um link de redefinição de senha foi enviado para seu e-mail")
                
            }).then(() => {window.history.back()})
            .catch (error => alert("E-mail inválido") )
        }
        else{
            console.error('formulário invalido')
        }  
    }
}

export default Forgotten;