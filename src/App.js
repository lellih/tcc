import './App.css';
import React from 'react';
import Routes from './Services/Routes';

export default class App extends React.Component {
  render(){
    

    return (
      <div className="App">
        <Routes/>
      </div>
    );
  }
}
